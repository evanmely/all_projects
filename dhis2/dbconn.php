<?php
header("content-Type:application/json");
define('host','127.0.0.1');
define('username', 'root');
define('password', '');
define('dbname', 'dhis2');
$conn=new mysqli(host, username, password, dbname);
if (!$conn){
    die("connection failed :".$conn->error);
}
$query= sprintf("SELECT * FROM dhis ORDER BY region");
$result=$conn->query($query);
$data=array();
foreach($result as $row)
    {
    $data[]=$row;
}
$result->close();
$conn->close();
print json_encode($data);
?>
        