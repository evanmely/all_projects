<!DOCTYPE HTML>
<html>
<head>
    <title>ChartJS - BarGraph</title>
		<style type="text/css">
			#chart-container {
				width: 640px;
				height: auto;
			}
		</style>
	</head>
	<body>
		<div id="chart-container">
			<canvas id="mycanvas"></canvas>
		</div>
        <script>
            window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer", {
        title:{
		   title :{
        text: "Multiple Y Axis Chart"
    },
    axisY:[
        {
            title: " Axis Y 1 Title",
        },
        {
            title: " Axis Y 2 Title",
        }
    ]           
		},
		data: [              
		{
			// Change type to "doughnut", "line", "splineArea", etc.
			type: "line",
                        axisYindex: 0,
			dataPoints: [
				{ label: "HTC",  y: 10  },
				{ label: "Immunization", y: 20  },
				{ label: "Family Planning", y: 25  },
				{ label: "Maternal Deaths",  y: 10  },
				{ label: "Medical Check-up",  y: 36  }
			]
		},
                {
			// Change type to "doughnut", "line", "splineArea", etc.
			type: "line",
                        axisYindex: 1,
			dataPoints: [
				{ label: "HTC",  y: 13  },
				{ label: "Immunization", y: 23  },
				{ label: "Family Planning", y: 30  },
				{ label: "Maternal Deaths",  y: 7  },
				{ label: "Medical Check-up",  y: 34  }
			]
		}
		]
	});
	chart.render();
}
</script>
  
<div id="chartContainer" style="height: 400px; width: 60%;"></div>
     <script  src="resource/jquery.min.js"></script>
    <script src="resource/js/bootstrap.min.js"></script>
     <script src="resource/chart.min.js"></script>
     <script src="resource/canvasjs.min.js"></script>
     <script src="app.js"></script>
</body>
</html>

