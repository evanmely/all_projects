$(document).ready(function(){
	$.ajax({
		url : "http://localhost/PhpProject1/dbconn.php",
		type : "GET",
		success : function(data){
			console.log(data);

			var region = [];
			var familyp = [];
			var htcp = [];
			var immp = [];
                        var maternal=[];
                        var checkup=[];

			for(var i in data) {
				region.push("Region " + data[i].region);
				familyp.push(data[i].familyp);
				htcp.push(data[i].htcp);
				immp.push(data[i].immp);
                                maternal.push(data[i].maternal);
                                checkup.push(data[i].checkup);
			}

			var chartdata = {
				labels: region,
				datasets: [
					{
						label: "familyp",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(59, 89, 152, 0.75)",
						borderColor: "rgba(59, 89, 152, 1)",
						pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
						pointHoverBorderColor: "rgba(59, 89, 152, 1)",
						data: familyp
					},
					{
						label: "htc patients",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(29, 202, 255, 0.75)",
						borderColor: "rgba(29, 202, 255, 1)",
						pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
						pointHoverBorderColor: "rgba(29, 202, 255, 1)",
						data: htcp
					},
                                        {
						label: "immunization no.",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(29, 202, 255, 0.75)",
						borderColor: "rgba(29, 202, 255, 1)",
						pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
						pointHoverBorderColor: "rgba(29, 202, 255, 1)",
						data: immp
					},
                                        {
						label: "maternal Deaths",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(29, 202, 255, 0.75)",
						borderColor: "rgba(29, 202, 255, 1)",
						pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
						pointHoverBorderColor: "rgba(29, 202, 255, 1)",
						data: maternal
					},
					{
						label: "checkup visits",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(211, 72, 54, 0.75)",
						borderColor: "rgba(211, 72, 54, 1)",
						pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
						pointHoverBorderColor: "rgba(211, 72, 54, 1)",
						data: checkup
					}
				]
			};

			var ctx = $("#mycanvas");

			var LineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata
			});
		},
		error : function(data) {

		}
	});
});



