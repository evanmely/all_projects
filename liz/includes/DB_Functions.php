<?php 
class DB_Functions {
 
    private $conn;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }
 
    
    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($name,$email) {
    
        $stmt = $this->conn->prepare("INSERT INTO invoice_user(name,email) VALUES(?,?)");
        $stmt->bind_param("ss",$name, $email);
        $result = $stmt->execute();
        $stmt->close();
 
        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM invoice_user WHERE email =?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
 
            return $user;
        } else {
            return false;
        }
    }

 }
?>