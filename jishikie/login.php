<?php

header('Content-Type: application/json');

 include_once("config.php");

 include_once("db_functions.php");

 include_once("customer.php");
 
// json response array
$response = array("error" => FALSE);

 
if (isset($_POST['name']) && isset($_POST['amount'])) {

 
    // receiving the post params
    $name = $_POST['name'];

    $amount = $_POST['amount'];

 
    // get the item by name and amount
    $item = $db->store_item($name, $amount);
    if ($item) {

        // item stored successfully
        $response["error"] = FALSE;

        $response["id"] = $item["id"];

        $response["item"]["name"] = $item["name"];

        $response["item"]["email"] = $item["amount"];
        
       
        echo json_encode($response);






        if ($user) {
            // user stored successfully
            $response["error"] = FALSE;
            $response["uid"] = $user["unique_id"];
            $response["user"]["name"] = $user["name"];
            $response["user"]["email"] = $user["email"];
            $response["user"]["phone"] = $user["phone"];
            $response["user"]["created_at"] = $user["created_at"];
            $response["user"]["updated_at"] = $user["updated_at"];
            echo json_encode($response);
        } else {
            // user failed to store
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknown error occurred in registration!";
            echo json_encode($response);
        }


    } else {
        // item is not found with the credentials

        $response["error"] = TRUE;

        $response["error_msg"] = "item not found sorry";

        echo json_encode($response);

    }
} else {
    // required post params is missing
    $response["error"] = TRUE;

    $response["error_msg"] = "Required parameters name or amount is missing!";

    echo json_encode($response);

}

?>