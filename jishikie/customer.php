<?php

header('Content-Type: application/json');


 include_once("config.php");

 include_once("db_functions.php");

 
 
// json response array
$response = array("error" => FALSE);

 
if (isset($_POST['name']) && isset($_POST['email'])) {

 
    // receiving the post params
    $name = $_POST['name'];

    $email = $_POST['email'];

     $phone = $_POST['phone'];

      $location = $_POST['location'];

 
    // get the item by name and amount
    $customer = $db->getCustomerByNameAndemail($name, $email);

 
    if ($customer) {

        // item stored successfully
        $response["error"] = FALSE;
        $response["customer"]['name'] = $customer['name'];
        $response["customer"]['email'] = $customer['email'];
        $response["customer"]['phone'] = $customer['phone'];
        
       $responce["customer"]['location']=$customer['location'];
       
        echo json_encode($response);

    } else {
        // error occured during login
        $response["error"] = TRUE;
        $response["error_msg"] = "error occured during login";
        echo json_encode($response);

    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters  are missing!";
    echo json_encode($response);

}

?>