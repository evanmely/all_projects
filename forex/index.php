<!DOCTYPE html>
<html xmlns:h="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewpoint" content="width=device-width,initial-scale=1">
  <title></title>
  <script src="resources/jquery.min.js"></script>
  <script src="resources/js/popper.min.js"></script>
  <script src="resources/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="resources/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="styles/styles.css">
  <link rel="stylesheet" type="text/css" href="resources/font-awesome/css/font-awesome.min.css">
</head>
<div  id="nav">
  <ul>
  <li><a href="">Home</a></li>
  <li><a href="">About Us</a></li>
  <li><a href="">Contact Us</a></li>
  <li><a href="">Platforms</a></li>
  <li><a href="analysis.php">Analysis</a></li>
  <li style="float:right"><a href="login.php">Login</a></li>
</ul>
</div>
<br><br>
<iframe style="width:100% ;height:150px;"src="rates.php"></iframe>

<body>
<br>
<br>
<div class="rates-bar">
</div>
<div class="container mt-3">
<div id="myCarousel" class="carousel slide">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li class="item1 active"></li>
    <li class="item2"></li>
    <li class="item3"></li>
    <li class="item4"></li>
    <li class="item5"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/forex2.jpg" alt="Los Angeles" width="1100" height="500">
       <div class="carousel-caption">
        <h3>Our Services</h3>
        <p class="lead">Real-Time Forex charts </p>
         <p>Current Rates</p>
        <p>Indicators</p>
                </div>
    </div>
    <div class="carousel-item">
      <img src="images/forex1.jpg" alt="Chicago" width="1100" height="500">
       <div class="carousel-caption" align="center">
        <h3>Forex  Calender</h3>
        <p>How to read Forex Calender</p>
         <p>Interpreting Forex Calender</p>
         <p>How to use it</p>
                </div>
    </div>
    <div class="carousel-item">
      <img src="images/forex3.jpg" alt="Chicago" width="1100" height="500">
       <div class="carousel-caption" >
        <h3>Forex Education And Mentorship</h3>
        <p>At the end,you should bes able to:</p>
         <p>Master Technical Analysis</p>
         <p>Build a winning trading system</p>
         <p>Trade with excellence</p>
         <p>Know FundamentalAnalysis</p>
         <p> Major currency Fundamentals</p>
         
                </div>
    </div>
    <div class="carousel-item">
      <img src="images/forex4.jpg" alt="Chicago" width="1100" height="500">
       <div class="carousel-caption">
           <p></p>
           <p></p>
            <p></p>
           <p></p>
        <h3>Platforms</h3>
        <p>We also guide you to get access to :</p>
         <p>Forex platforms for windows,android,IOS</p>
          <p>MetaTrader 4</p>
           <p>MetaTrader 5</p>
           <p>How to use them</p>
           
                </div>
    </div>
    <div class="carousel-item">
      <img src="images/forex5.jpg" alt="New York" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Analysis and Forum</h3>
        <p>Get Acess to Forex Daily Analysis!</p>
         <p>Discussion forum with other worldwide traders!</p>
          <p>                   </p>
          <p>                   </p>
          <p>                   </p>
          <p>                   </p>
                </div>
    </div>
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#myCarousel">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#myCarousel">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
</div>

<script>
$(document).ready(function(){
    // Activate Carousel
    $("#myCarousel").carousel();
    
    // Enable Carousel Indicators
    $(".item1").click(function(){
        $("#myCarousel").carousel(0);
    });
    $(".item2").click(function(){
        $("#myCarousel").carousel(1);
    });
    $(".item3").click(function(){
        $("#myCarousel").carousel(2);
    });
    $(".item4").click(function(){
        $("#myCarousel").carousel(3);
    });
    $(".item5").click(function(){
        $("#myCarousel").carousel(4);
    });
    // Enable Carousel Controls
    $(".carousel-control-prev").click(function(){
        $("#myCarousel").carousel("prev");
    });
    $(".carousel-control-next").click(function(){
        $("#myCarousel").carousel("next");
    });
});
</script>
  <p text align="center">This website provides guidance on how to trade on forex and make enormous profits from it.</p> 
</body>
<div class="footer-main">
<footer>
 <div class="row">
  <div class="col-lg-3">
<p style="color:white" >TECHNICAL ANALYSIS</p>
<ul>
<li><a href="">Articles</a></li>
<li><a href="">Analyst Picks</a></li>
<li><a href="">Support & Resistance</a></li>
<li><a href="">Pivot Points</a></li>
<li><a href="">Sentiment</a></li>
</ul>
  </div>
  <div class="col-lg-3">
<p style="color:white" >Tools</p>
<ul>
     <li><a href="">FOREX TOOLS</a></li>
     <li><a href="">Forex Calendar</a></li>
     <li><a href="">Forex Rates</a></li>
</ul>
    
   </div>
  <div class="col-lg-3">

<p style="color:white" >Quick Links</p><ul>
<li><a href="">About</a></li>
<li><a href="">Contact</a></li>
<li><a href="">Advertise</a></li>
<li><a href="">Contribute</a></li>
<li><a href="">Newsletter</a></li>
<li><a href="">Testimonials</a></li>
<li><a href="">FAQ</a></li>
</ul>
  </div>
       <div class="col-lg-3">
    
  </div>

</div>
<p text align="center" style="color:white"><u> Disclaimer-High Risk Investment</u><br>
Trading forex exchange on margin carries high level of risk,and may not be  suitable for all investors.
Before deciding to trade foreign currency exchange,you should carefully consider your investment objectives ,
level of experience and risk appetite.The possibility exists that you could sustain a loss of some or all of your initial investment and so you should not invest in money that you cannot afford to loose.You should be aware of all the risks associated with foreign exchange trading and seek advice from an independent financial  advisor if you have any doubts.
</p>
</div>


<p text align="center" style="color:white">Follow us on social media </p>
    <div class="footer-social-icons">
  <ul>
    <li> <a href="#"><i class="fa fa-facebook"></i></a></li>
        <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
        <li> <a href="#"><i class="fa fa-google-plus"></i></a></li>
        <li> <a href="#"><i class="fa fa-telegram"></i></a></li>
  </ul>

</div>


</div>
<footer background-color="lightgreen">

    
  <p text align="center"><b>copyright &copy; 2018 Forex mentors<br>All Rights Reserved</b></p>  

</footer>

</html>