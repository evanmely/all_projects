
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<div class="tradingview-widget-container">
  <div id="tradingview_91b71"></div>
 <span class="blue-text">EURUSD chart</span></a></div>
  <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
  <script type="text/javascript">
  new TradingView.widget(
  {
  "width": 980,
  "height": 610,
  "symbol": "EURUSD",
  "interval": "D",
  "timezone": "exchange",
  "theme": "Dark",
  "style": "1",
  "locale": "en",
  "toolbar_bg": "#f1f3f6",
  "enable_publishing": false,
  "allow_symbol_change": true,
  "container_id": "tradingview_91b71"
}
  );
  </script>
</div>
</body>
</html>
