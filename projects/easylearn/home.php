<h1><?php echo $title;?></h1>

<div class="col-lg-12" id="About">  
<h4>About Easylearn</h4>
  <p>Easylearn<a href="">online high school  classes</a> is a great program for students to participate in that are in need of help outside of traditional classrooms, eLearning has a great many benefits. An <a href="">online tutor</a> educates students following a curriculum of that child’s home state, ensuring all work covered is the same as what is required by that student’s traditional school.</p>
<p>High school is a time of extreme importance to not fall behind on grades, with college is right around the corner. High school already has enough stresses, to be struggling is not something a parent wants for their child. If a student is struggling to maintain their grades or need a substitution for an class, <a href="">eLearning</a> is the solution. This is a way with less stress, where high school students enjoy the process of learning. Students work at their own pace, never rushed to move on to the next lesson before they are comfortable with what they learned.</p> 
</span>
</div>
<div  class="col-lg-6">
  <h4>Vision</h4> 
  <p>Center of Academic Excellence Delivering Quality Service to all.</p>
  <h4>Mission</h4>
  <p> Easylearn is committed to provide Compact skill and knowledge; develop skills, talents and values;</p>
  <h4>Goals</h4> 
  <ol>
    <li>Globally competitive graduates</li>  
    <li>Institutionalize research culture</li> 
    <li>Responsive and sustainable extension services</li> 
    <li>Maximized profit of variable agro-industrial business ventures</li>  
    <li>Effective and efficient administration </li>  
  </ol>

   <h4><strong>Online  Classes And What They Cover</strong></h4>
<p>Online classes with easylearn covers topics all topic in traditional classroom. An online tutor helps move a student along the road of learning. This tutoring program helps ensure these high school students are ready for college, by teaching them all needed topics.</p>
<p>By eLearning, students have the ability of learning at their own pace. If a student feels the need to redo a lesson, they have the ability to do so. Lessons are available online and off. Students choose what days and times they want to study, put down a lesson until later if want. There is nobody to pressure them, bringing a more relaxed learning environment to these students than traditional classrooms.</p>
<p>After finishing an <a href="">online  class</a> for high school, students have the confidence they need to proceed to the college of their dreams.</p>
</div>
<div  class="col-lg-6">
  <h4>Guiding Principles</h4> 
    <ul>
      <li>Academic Freedom </li>
      <li>Responsibility</li>
      <li> Academic Standards</li>  
    </ul>
<h4>Core Values</h4>
  <ul>
    <li>God-Centered</li>
    <li>Excellence</li>
    <li>Integrity</li>
    <li>Transparency and Accountability</li>
    <li>Dedication to Quality Service</li>  
  </ul> 
<h4>Quality Policy</h4> 
<ol>
    <li>Easylearn is committed to be the center of academic excellence delivering quality service to all by;</li> 
    <li>Continuing  quality improvement cultivating an efficient and effective and environment for maximum clientele satisfaction;</li> 
    <li>Adhering to laws and regulations, global standards, and environmental change requirements;</li>  
    <li>Showcasing quality outputs; and,</li> 
    <li>Upholding values and integrity and nurturing talents and skills for global competitiveness.</li>  
</ol>
</div>
 

