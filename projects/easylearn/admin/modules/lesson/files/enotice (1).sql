-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2019 at 08:53 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enotice`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '2',
  `password` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `gender`, `email`, `phone`, `type`, `password`) VALUES
(1, 'silas', 'Others', 'kinyangasilas@gmail.com', '000', 1, '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL,
  `departmentname` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `departmentname`) VALUES
(1, 'computer science'),
(3, 'informatio communication technology'),
(2, 'information technology');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE IF NOT EXISTS `notice` (
  `id` int(11) NOT NULL,
  `department` varchar(60) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `upl_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cl_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `department`, `title`, `content`, `upl_date`, `cl_date`) VALUES
(23, 'information comunication technology', 'qwerty', '<p>qwerty keybord invention!<img src="/projects/enotice/tinymce/plugins/emoticons/img/smiley-embarassed.gif" alt="embarassed" /></p>', '2019-02-22 11:12:47', '0000-00-00'),
(24, 'computer science', 'qwerty', '<p>qwerty keybord invention!<img src="/projects/enotice/tinymce/plugins/emoticons/img/smiley-embarassed.gif" alt="embarassed" /></p>', '2019-02-22 11:12:50', '0000-00-00'),
(25, 'information technology ', 'qwerty', '<p>qwerty keybord invention!<img src="/projects/enotice/tinymce/plugins/emoticons/img/smiley-embarassed.gif" alt="embarassed" /></p>', '2019-02-22 11:38:22', '0000-00-00'),
(26, 'computer science', 'qwerty', '<p>qwerty keybord invention!<img src="/projects/enotice/tinymce/plugins/emoticons/img/smiley-embarassed.gif" alt="embarassed" /></p>', '2019-02-22 11:41:44', '0000-00-00'),
(27, 'information technology', 'notice', '<p>hey come meet the vc</p>', '2019-03-04 15:39:18', '0000-00-00'),
(28, 'information technology', '13234', '<p>hellow dear</p>', '2019-03-06 13:42:23', '0000-00-00'),
(29, 'computer science', 'treryU', '<p>HEY</p>', '2019-03-06 13:42:46', '0000-00-00'),
(31, '', 'treryU', '<p>HEY come home</p>', '2019-03-13 15:40:05', '0000-00-00'),
(32, '', 'qwerty', '<p>qwerty keybord invention!there</p>', '2019-03-13 15:40:52', '0000-00-00'),
(33, 'informatio communication technology', 'finish', '<p><a href="http://stg.pz10139.parspack.net/Films/Hacker.2016/Hacker.2016.1080p.BluRay.6CH.Farsi.Dubbed-%5BBi-3-Seda.Ir%5D.mkv">Hacker.2016.1080p.BluRay.6CH.Farsi.Dubbed-[Bi-3..&gt;</a> 14-Jun-2017 04:09 1793085918 <a href="http://stg.pz10139.parspack.net/Films/Hacker.2016/Hacker.2016.720p.BluRay.2CH.x265.HEVC.Farsi.Dubbed-%5BBi-3-Seda.Ir%5D.mkv">Hacker.2016.720p.BluRay.2CH.x265.HEVC.Farsi.Dub..&gt;</a> 14-Jun-2017 04:08 532345228 <a href="http://stg.pz10139.parspack.net/Films/Hacker.2016/Hacker.2016.720p.BluRay.2CH.x265.HEVC.mkv">Hacker.2016.720p.BluRay.2CH.x265.HEVC.mkv</a> 14-Jun-2017 04:09 428178543 <a href="http://stg.pz10139.parspack.net/Films/Hacker.2016/Hacker.2016.720p.BluRay.Farsi.Dubbed-%5BBi-3-Seda.Ir%5D.mkv">Hacker.2016.720p.BluRay.Farsi.Dubbed-[Bi-3-Seda..&gt;</a> 14-Jun-2017 04:06 910632801 <a href="http://stg.pz10139.parspack.net/Films/Hacker.2016/Hacker.2016.Dubbed.Audio-%5BBi-3-Seda.Ir%5D.rar">Hacker.2016.Dubbed.Audio-[Bi-3-Seda.Ir].rar</a> 14-Jun-2017 04:06 84427274 <a href="http://stg.pz10139.parspack.net/Films/Hacker.2016/Hacker_-_Trailer-%5B360p%5D.MP4">Hacker_-_Trailer-[360p].MP4</a> 14-Jun-2017 04:06 9218744</p>', '2019-03-15 02:38:41', '1990-12-05'),
(34, 'information technology', 'finish', '<p>$success;</p>', '2019-03-15 02:44:07', '2019-12-23'),
(35, 'informatio communication technology', 'finish', '<p style="text-align: justify;">Henry Dalziel is a serial education entrepreneur, founder of Concise Ac<br />Ltd, online cybersecurity blogger, and e-book author. He writes for the<br />Concise-Courses.com blog and has developed numerous cybersecurity<br />continuing education courses and books. Concise Ac Ltd develops and<br />distributes continuing education content [books and courses] for<br />cybersecurity professionals seeking skill enhancement and career<br />advancement. The company was recently accepted onto the UK Trade<br />&amp; Investment&rsquo;s (UKTI) Global Entrepreneur Programme (GEP).<br />Kevin Cardwell works as a freelance consultant and provides<br />consulting services for companies throughout the world, and as an<br />advisor to numerous government entities within the US, Middle East,<br />Africa, Asia, and the UK. He is an Instructor, Technical Editor and<br />Author for Computer Forensics, and Hacking courses. He is author of<br />Building Virtual Pentesting Labs for Advanced Penetration Testing.<br />Currently providing consultancy to ASM on curriculum development<br />and information security projects in security for Government clients<br />within the US.</p>', '2019-03-15 02:48:03', '2019-12-31'),
(36, '', 'qwerty', '<p>qwerty keybord invention!there</p>\r\n<p>Henry Dalziel is a serial education entrepreneur, founder of Concise Ac<br />Ltd, online cybersecurity blogger, and e-book author. He writes for the<br />Concise-Courses.com blog and has developed numerous cybersecurity<br />continuing education courses and books. Concise Ac Ltd develops and<br />distributes continuing education content [books and courses] for<br />cybersecurity professionals seeking skill enhancement and career<br />advancement. The company was recently accepted onto the UK Trade<br />&amp; Investment&rsquo;s (UKTI) Global Entrepreneur Programme (GEP).<br />Kevin Cardwell works as a freelance consultant and provides<br />consulting services for companies throughout the world, and as an<br />advisor to numerous government entities within the US, Middle East,<br />Africa, Asia, and the UK. He is an Instructor, Technical Editor and<br />Author for Computer Forensics, and Hacking courses. He is author of<br />Building Virtual Pentesting Labs for Advanced Penetration Testing.<br />Currently providing consultancy to ASM on curriculum development<br />and information security projects in security for Government clients<br />within the US.</p>', '2019-03-15 02:48:47', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `admission` varchar(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `department` varchar(30) NOT NULL,
  `year` int(4) NOT NULL,
  `password` varchar(60) NOT NULL,
  `hostel` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `picture` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `username`, `admission`, `name`, `gender`, `department`, `year`, `password`, `hostel`, `email`, `phone`, `picture`) VALUES
(1, 'ci/00072/015', 'ci/00072/015', 'emilys', 'Male', 'Computer Science', 2, '356a192b7913b04c54574d18c28d46e6395428ab', 'mak box', 'MQIN1', '0790799111', '../images/avatar.png'),
(2, 'ci/00051/015', 'ci/00051/015', 'kinyanga silas', 'Male', 'information technology ', 4, '356a192b7913b04c54574d18c28d46e6395428ab', 'makerere', 'kinyangasilas@gmail.com', '0790788100', '../images/avatar.png'),
(3, 'ci/00073/015', 'ci/00073/015', 'emmah', 'Female', 'Information Technology', 4, '356a192b7913b04c54574d18c28d46e6395428ab', 'mak box', 'e@gmail.com', '0702344343', '../images/avatar.png'),
(4, 'ci/00071/015', 'ci/00071/015', 'dorcas', 'Male', 'Computer Science', 4, '356a192b7913b04c54574d18c28d46e6395428ab', 'makerere', 'dorcas@gmail.com', '000978845', '../images/avatar.png'),
(5, 'MT/00291/015', 'MT/00291/015', 'judy', 'Female', 'Computer Science', 3, '356a192b7913b04c54574d18c28d46e6395428ab', 'makerere', 'judy@gmail.com', '12234567', '../images/avatar.png'),
(6, 'ci/00056/015', 'ci/00056/015', 'silas', 'Male', 'Computer Science', 2, '7b52009b64fd0a2a49e6d8a939753077792b0554', 'makerere', 'kinyangasilas1@gmail.com', '000', '../images/avatar.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departmentname` (`departmentname`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admission` (`admission`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
