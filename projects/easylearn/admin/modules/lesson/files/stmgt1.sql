CREATE TABLE IF NOT EXISTS `accountmaster` (
  `ID` int(11) NOT NULL,
  `UID` varchar(10) NOT NULL,
  `FirstName` varchar(20) NOT NULL,
  `LastName` varchar(20) NOT NULL,
  `MiddleName` varchar(20) NOT NULL,
  `Type` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `phonelist` (
  `ID` int(11) NOT NULL,
  `Contact_ID` int(11) NOT NULL,
  `PhoneNumber` varchar(15) NOT NULL,
  FOREIGN KEY(Contact_ID) REFERENCES accountmaster(ID)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
