-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2019 at 05:57 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enotice`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '2',
  `password` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `gender`, `email`, `phone`, `type`, `password`) VALUES
(1, 'a', 'a', 'e', 'a', 1, '356a192b7913b04c54574d18c28d46e6395428ab'),
(2, '12', 'Male', '12121@gmail.com', '122', 1, '356a192b7913b04c54574d18c28d46e6395428ab'),
(3, 'joy', 'Male', 'ken@gm', '0-009', 1, '356a192b7913b04c54574d18c28d46e6395428ab'),
(4, 'kinyanga silas', 'Male', 'kinyangasilas@gmail.com', '0790788100', 1, '7b52009b64fd0a2a49e6d8a939753077792b0554'),
(5, 'kinyanga silas', 'Male', 'emily@yahoo.com', '12134', 1, '356a192b7913b04c54574d18c28d46e6395428ab'),
(6, 'kinyanga silas', 'Female', 'kinyangasilas@gmail.com', '0790788100', 1, '7b52009b64fd0a2a49e6d8a939753077792b0554'),
(7, 'emily', 'Female', 'kinyangasilas@gmail.com', '0790788100', 1, '356a192b7913b04c54574d18c28d46e6395428ab'),
(8, 'dorcas@gmail.com', 'Female', 'dorcas@gmail.com', '1234', 1, '356a192b7913b04c54574d18c28d46e6395428ab'),
(9, 'dorcas', 'Female', 'dorcas@gmail.com', '12344', 1, '356a192b7913b04c54574d18c28d46e6395428ab'),
(10, 'emmah', 'Female', 'emmah@gmail.com', '0712321234', 1, '356a192b7913b04c54574d18c28d46e6395428ab'),
(11, 'doky', 'Female', 'doky@gmail.com', '123456789', 1, '356a192b7913b04c54574d18c28d46e6395428ab');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `departmentname` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `departmentname`) VALUES
(1, 'computer science'),
(3, 'informatio communication technology'),
(2, 'information technology');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `department` varchar(60) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `upl_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cl_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `department`, `title`, `content`, `upl_date`, `cl_date`) VALUES
(23, 'information comunication technology', 'qwerty', '<p>qwerty keybord invention!<img src=\"/projects/enotice/tinymce/plugins/emoticons/img/smiley-embarassed.gif\" alt=\"embarassed\" /></p>', '2019-02-22 11:12:47', '0000-00-00'),
(24, 'computer science', 'qwerty', '<p>qwerty keybord invention!<img src=\"/projects/enotice/tinymce/plugins/emoticons/img/smiley-embarassed.gif\" alt=\"embarassed\" /></p>', '2019-02-22 11:12:50', '0000-00-00'),
(25, 'information technology ', 'qwerty', '<p>qwerty keybord invention!<img src=\"/projects/enotice/tinymce/plugins/emoticons/img/smiley-embarassed.gif\" alt=\"embarassed\" /></p>', '2019-02-22 11:38:22', '0000-00-00'),
(26, 'computer science', 'qwerty', '<p>qwerty keybord invention!<img src=\"/projects/enotice/tinymce/plugins/emoticons/img/smiley-embarassed.gif\" alt=\"embarassed\" /></p>', '2019-02-22 11:41:44', '0000-00-00'),
(27, 'information technology', 'notice', '<p>hey come meet the vc</p>', '2019-03-04 15:39:18', '0000-00-00'),
(28, 'information technology', '13234', '<p>hellow dear</p>', '2019-03-06 13:42:23', '0000-00-00'),
(29, 'computer science', 'treryU', '<p>HEY</p>', '2019-03-06 13:42:46', '0000-00-00'),
(31, '', 'treryU', '<p>HEY come home</p>', '2019-03-13 15:40:05', '0000-00-00'),
(32, '', 'qwerty', '<p>qwerty keybord invention!there</p>', '2019-03-13 15:40:52', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `admission` varchar(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `department` varchar(30) NOT NULL,
  `year` int(4) NOT NULL,
  `password` varchar(60) NOT NULL,
  `hostel` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `picture` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `username`, `admission`, `name`, `gender`, `department`, `year`, `password`, `hostel`, `email`, `phone`, `picture`) VALUES
(1, 'ci/00072/015', 'ci/00072/015', 'emilys', 'Male', 'Computer Science', 2, '356a192b7913b04c54574d18c28d46e6395428ab', 'mak box', 'MQIN1', '0790799111', '../images/avatar.png'),
(2, 'ci/00051/015', 'ci/00051/015', 'kinyanga silas', 'Male', 'information technology ', 4, '356a192b7913b04c54574d18c28d46e6395428ab', 'makerere', 'kinyangasilas@gmail.com', '0790788100', '../images/avatar.png'),
(3, 'ci/00073/015', 'ci/00073/015', 'emmah', 'Female', 'Information Technology', 4, '356a192b7913b04c54574d18c28d46e6395428ab', 'mak box', 'e@gmail.com', '0702344343', '../images/avatar.png'),
(4, 'ci/00071/015', 'ci/00071/015', 'dorcas', 'Male', 'Computer Science', 4, '356a192b7913b04c54574d18c28d46e6395428ab', 'makerere', 'dorcas@gmail.com', '000978845', '../images/avatar.png'),
(5, 'MT/00291/015', 'MT/00291/015', 'judy', 'Female', 'Computer Science', 3, '356a192b7913b04c54574d18c28d46e6395428ab', 'makerere', 'judy@gmail.com', '12234567', '../images/avatar.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departmentname` (`departmentname`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admission` (`admission`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
