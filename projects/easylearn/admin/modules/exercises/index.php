<?php
require_once("../../../include/initialize.php");
if(!isset($_SESSION['USERID'])){
	redirect(web_root."admin/index.php");
}

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';
$success = (isset($_GET['q']) && $_GET['q'] != '') ? $_GET['q'] : '';
$type =(isset($_GET['type']) && $_GET['type'] != '') ? $_GET['type'] : '';
switch ($view) {
	case 'mark':
		$content	= 'mark.php';
		break;
	case 'award':
		$content	= 'award.php';
		break;
	case 'list' :
		$content    = 'list.php';		
		break;

	case 'add' :
		$content    = 'add.php';		
		break;

	case 'edit' :
		$content    = 'edit.php';		
		break;
    case 'view' :
		$content    = 'view.php';		
		break;

	default :
		$content    = 'list.php';		
}
require_once("../../navigation/navigations.php");
?>
  
