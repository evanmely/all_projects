<?php 
require_once("../../../include/initialize.php");
if(!isset($_SESSION['USERID'])){
	redirect(web_root."admin/index.php");
}
$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';
$file = (isset($_GET['Answer']) && $_GET['Answer'] != '') ? $_GET['Answer'] : '';
$exerciseid =(isset($_GET['exerciseid']) && $_GET['exerciseid'] != '') ? $_GET['exerciseid'] : '';
$studentid= (isset($_GET['studentid']) && $_GET['studentid'] != '') ? $_GET['studentid'] : '';
$lessonid= (isset($_GET['lessonid']) && $_GET['lessonid'] != '') ? $_GET['lessonid'] : '';
if (isset($_POST['award'])) {
  $marks =$_POST['marks'];
  $lid =$_POST['lessonid'];
  $sid =$_POST['studentid'];
  $eid =$_POST['exerciseid'];
  $sql = "Update tblstudentquestion Set marked = 1 WHERE LessonID='{$lid}' and StudentID='{$sid}' and ExerciseID ='{$eid}'";
$mydb->setQuery($sql);
$mydb->executeQuery();
$sql = "SELECT * From tblpartbscore WHERE ExerciseID = '{$eid}' AND StudentID='{$sid}'";
$mydb->setQuery($sql);
$row = $mydb->executeQuery();
$maxrow = $mydb->num_rows($row);

if ($maxrow>0) { 
  $sql = "UPDATE tblpartscore SET Score='{$marks}' WHERE ExerciseID = '{$eid}' AND StudentID='{$sid}'";  
  $mydb->setQuery($sql);
  $mydb->executeQuery();

}else{ 
  $sql = "INSERT INTO tblpartbscore (`LessonID`,`ExerciseID`, `StudentID`, `Score`,`Submitted`) VALUES ('{$lid}','{$eid}','{$sid}','{$marks}','1')";
  $mydb->setQuery($sql);
  $mydb->executeQuery(); 
}
// message("Exercises already submitted.","sucess");
redirect("index.php?view=mark&&q=success");

}

 ?>
 <h2>View PDF File</h2>
 <style type="text/css">
   .limiter {
  width: 100%;
  margin: 0 auto;
}

.container-login100 {
  width: 100%;  
  min-height: 100vh;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  padding: 15px;
 /* background: #ddd;
  background: -webkit-linear-gradient(-135deg, #c850c0, #4158d0);
  background: -o-linear-gradient(-135deg, #c850c0, #4158d0);
  background: -moz-linear-gradient(-135deg, #c850c0, #4158d0);
  background: linear-gradient(-135deg, #c850c0, #4158d0);*/
}

.wrap-login100 {
  width: 960px;
  background: #fff;
  border-radius: 10px;
  overflow: hidden;

  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 177px 130px 33px 95px;
}

/*------------------------------------------------------------------
[  ]*/
.login100-pic {
  width: 316px;
}

.login100-pic img {
  max-width: 100%;
}


/*------------------------------------------------------------------
[  ]*/
.login100-form {
  width: 290px;
}

.login100-form-title {
  font-family: Poppins-Bold;
  font-size: 24px;
  color: #333333;
  line-height: 1.2;
  text-align: center;

  width: 100%;
  display: block;
  padding-bottom: 54px;
}


/*---------------------------------------------*/
.wrap-input100 {
  position: relative;
  width: 100%;
  z-index: 1;
  margin-bottom: 10px;
}

.input100 {
  font-family: Poppins-Medium;
  font-size: 15px;
  line-height: 1.5;
  color: #666666;

  display: block;
  width: 100%;
  background: #e6e6e6;
  height: 50px;
  border-radius: 25px;
  padding: 0 30px 0 68px;
}


/*------------------------------------------------------------------
[ Focus ]*/
.focus-input100 {
  display: block;
  position: absolute;
  border-radius: 25px;
  bottom: 0;
  left: 0;
  z-index: -1;
  width: 100%;
  height: 100%;
  box-shadow: 0px 0px 0px 0px;
  color: rgba(87,184,70, 0.8);
}

.input100:focus + .focus-input100 {
  -webkit-animation: anim-shadow 0.5s ease-in-out forwards;
  animation: anim-shadow 0.5s ease-in-out forwards;
}

@-webkit-keyframes anim-shadow {
  to {
    box-shadow: 0px 0px 70px 25px;
    opacity: 0;
  }
}

@keyframes anim-shadow {
  to {
    box-shadow: 0px 0px 70px 25px;
    opacity: 0;
  }
}

.symbol-input100 {
  font-size: 15px;

  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  align-items: center;
  position: absolute;
  border-radius: 25px;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  padding-left: 35px;
  pointer-events: none;
  color: #666666;

  -webkit-transition: all 0.4s;
  -o-transition: all 0.4s;
  -moz-transition: all 0.4s;
  transition: all 0.4s;
}

.input100:focus + .focus-input100 + .symbol-input100 {
  color: #57b846;
  padding-left: 28px;
}

/*------------------------------------------------------------------
[ Button ]*/
.container-login100-form-btn {
  width: 100%;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding-top: 20px;
}

.login100-form-btn {
  font-family: Montserrat-Bold;
  font-size: 15px;
  line-height: 1.5;
  color: #fff;
  text-transform: uppercase;

  width: 100%;
  height: 50px;
  border-radius: 25px;
  background: #57b846;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 25px;

  -webkit-transition: all 0.4s;
  -o-transition: all 0.4s;
  -moz-transition: all 0.4s;
  transition: all 0.4s;
}

.login100-form-btn:hover {
  background: #333333;
}



/*------------------------------------------------------------------
[ Responsive ]*/



@media (max-width: 992px) {
  .wrap-login100 {
    padding: 177px 90px 33px 85px;
  }

  .login100-pic {
    width: 35%;
  }

  .login100-form {
    width: 100%;
  }
}

@media (max-width: 768px) {
  .wrap-login100 {
    padding: 100px 80px 33px 80px;
  }

  .login100-pic {
    display: none;
  }

  .login100-form {
    width: 100%;
  }
}

@media (max-width: 576px) {
  .wrap-login100 {
    padding: 100px 15px 33px 15px;
  }
}

 </style>
 <?php if ($file !="") {
  ?>
  <div class="container ">
  <embed src="<?php echo web_root.'partB/'.$file; ?>" type="application/pdf" style="height: 100vh;width: 100%;" />
</div> 
<form class="login100-form validate-form" action="award.php?view=award" method="POST"> 
          <span class="login100-form-title">
            Add Result
          </span>

          <div class="wrap-input100 validate-input" >
            <input class="input100" type="number" name="marks" placeholder="Marks">
            <input type="hidden" name="exerciseid" value="<?php echo($exerciseid)  ?>">
            <input type="hidden" name="lessonid" value="<?php echo($lessonid)  ?>">
            <input type="hidden" name="studentid" value="<?php echo($studentid)  ?>">
               </div>
        
          <div class="container-login100-form-btn">
            <button class="login100-form-btn" type="submit" name="award">
              Submit
            </button>
          </div>
        </form>
  <?php

 } ?>
