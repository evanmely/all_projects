 <?php 
 if (!isset($_SESSION['USERID'])){
      redirect(web_root."admin/index.php");
     }
?> 
      <script type="text/javascript" src="<?php echo web_root; ?>tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
    tinymce.init({
    selector: '#Questiont2',
    theme: 'modern', 
    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    },
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor code autosave'
    ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
  image_caption: true,
 relative_urls : false,
remove_script_host : true,
//document_base_url : "http://radiotech.champyouths254.co.ke/merutv/user/editor/"  
  });
  </script>
                     <form class="form-horizontal span6" action="controller.php?action=add" method="POST" style="margin-top: 20px;"> 
                        <div class="row">
                           <div class="col-lg-12">
                              <h1 class="page-header">Add New Question</h1>
                            </div>
                            <!-- /.col-lg-12 -->
                         </div>
                        <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "Lesson">Lesson:</label>

                          <div class="col-md-8"> 
                            <select class="form-control" name="Lesson">
                              <?php 
                               $sql = "SELECT * FROM `tbllesson`";
                               $mydb->setQuery($sql);
                               $cur = $mydb->loadResultList();
                               foreach ($cur as $res) {
                                 # code...
                                echo '<option value='.$res->LessonID.'>'.$res->LessonTitle.'</option>';
                               }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div> 


                      <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "Question">Question Type:</label>
                           <div class="col-md-8">
                            <select name="Questiontype" class="form-control input-sm" onchange="changeUrl(this.value)">
                              <?php
                              if ($type == "Multichoice") {
                               ?>
                               <option value="Multichoice">Multichoice</option>
                              <option value="Other">Others</option>
                               <?php
                              }
                              else
                              {
                              ?>
                              <option value="Other">Others</option>
                               <option value="Multichoice">Multichoice</option>
                               <?php
                             }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <?php
                      if ($type == "Multichoice") 
                      {
                      ?>

                       <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "Question">Question:</label>

                          <div class="col-md-8">
                            <textarea  class="form-control input-sm" id="Question" name="Question" placeholder=
                                "Question Name" type="text"></textarea>
                            
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "ChoiceA">A:</label>

                          <div class="col-md-8">
                            
                             <input class="form-control input-sm" id="ChoiceA" name="ChoiceA" placeholder=
                                "" type="text" value="">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "ChoiceB">B:</label>

                          <div class="col-md-8">
                            
                             <input class="form-control input-sm" id="ChoiceB" name="ChoiceB" placeholder=
                                "" type="text" value="" required>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "ChoiceC">C:</label>

                          <div class="col-md-8">
                            
                             <input class="form-control input-sm" id="ChoiceC" name="ChoiceC" placeholder=
                                "" type="text" value="" required>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "ChoiceD">D:</label>

                          <div class="col-md-8">
                              <input class="form-control input-sm" id="ChoiceD" name="ChoiceD" placeholder=
                                "" type="text" value="" required>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "Answer">Answer:</label>

                          <div class="col-md-8">
                            
                             <input class="form-control input-sm" id="Answer" name="Answer" placeholder=
                                "Answer" type="text" value="" required>
                          </div>
                        </div>
                      </div> 
                    <?php
                    }
                    else
                    {
                     ?>
                      <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "Question">Question:</label>
                          <div class="col-md-12">
                            <textarea  class="form-control input-sm" id="Questiont2" name="Question" placeholder="Question Name" type="text" rows="20" ></textarea>
                            
                          </div>
                        </div>
                      </div>
                                            <div class="form-group">
                        <div class="col-md-8">

                          <div class="col-md-8">
                            
                             <input class="form-control input-sm" id="ChoiceA" name="ChoiceA" placeholder=
                                "" type="hidden" value="">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-8">

                          <div class="col-md-8">
                            
                             <input class="form-control input-sm" id="ChoiceB" name="ChoiceB" placeholder=
                                "" type="hidden" value="" >
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-8">

                          <div class="col-md-8">
                            
                             <input class="form-control input-sm" id="ChoiceC" name="ChoiceC" placeholder=
                                "" type="hidden" value="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-8">

                          <div class="col-md-8">
                              <input class="form-control input-sm" id="ChoiceD" name="ChoiceD" placeholder=
                                "" type="hidden" value="" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-8">
                            
                             <input class="form-control input-sm" id="Answer" name="Answer" placeholder=
                                "Answer" type="hidden" value="" >
                          </div>
                        </div>
                      </div> 

                     <?php
                      }
                    ?>

                      <div class="form-group">
                        <div class="col-md-8">
                          <label class="col-md-4 control-label" for=
                          "idno"></label>

                          <div class="col-md-12">
                           <button class="btn btn-primary btn-lg" name="save" type="submit" ><span class="fa fa-save fw-fa"></span>  Save</button> 
                           </div>
                        </div>
                      </div> 
                      </form>
                      <script type="text/javascript">
                        function changeUrl(value) {
                          window.location.href ="index.php?view=add&&type="+value;
                                }
                      </script>