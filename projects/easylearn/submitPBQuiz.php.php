<?php 
require_once ("include/initialize.php");
	if(!isset($_SESSION['USERID'])){
	redirect(web_root."index.php");
}
if (isset($_GET['id']) && $_SESSION["questions"] !=null) {
	array_splice($_SESSION["questions"],$_GET['id']);
	redirect("index.php?q=question&id={$_GET['ls']}");
}
$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch ($action) {
	case 'add' :
	doInsert();
	break;
 
	}
function doInsert(){ 
	global $mydb;
		if(isset($_POST['btnSubmit'])){ 
			$studentid = $_SESSION['StudentID'];
			$exersiceid = $_POST['ExerciseID'];
			$lessonid = $_POST['LessonID'];
			$filename = UploadImage($studentid,$exersiceid,$lessonid);
			$location = "partB/". $filename ;
			$answer =$location;
			$score =0;
	$sql = "SELECT * From tblpartbscore WHERE ExerciseID = '{$exersiceid}' AND StudentID='{$studentid}'";
	$mydb->setQuery($sql);
	$row = $mydb->executeQuery();
	$maxrow = $mydb->num_rows($row);
	if ($maxrow>0) { 
		$sql = "UPDATE tblpartbscore SET Score='{$score}' WHERE ExerciseID = '{$exersiceid}' AND StudentID='{$studentid}'";  
		$mydb->setQuery($sql);
		$mydb->executeQuery();

	}else{ 
		$sql = "INSERT INTO tblpartbscore (`LessonID`,`ExerciseID`, `StudentID`, `answer`,`Score`) VALUES ('{$lessonid}','{$exersiceid}','{$studentid}','{$answer}','{$score}')";
		$mydb->setQuery($sql);
		$mydb->executeQuery(); 
	}
	if (isset($_SESSION['questions'])) {
		array_push($_SESSION['questions'] , $exersiceid);
	}else{
	$_SESSION['questions'] =array();
	array_push($_SESSION['questions'], $exersiceid);
	}

			message("Answer has been saved in the database.", "success");
			redirect("index.php?q=question&id={$lessonid}");			
		}  
	}
function dochangefile(){
		if(isset($_POST['save'])){   
				$id = $_POST['LessonID'];
 
				$filename = UploadImage();
				$location = "partB/". $filename ;

				$lesson = new Lesson(); 
				$lesson->FileLocation  = $location;
				$lesson->update($id); 

				message("File has been updated in the database.", "success");
				redirect("index.php");
		 

			
	 		
		}
	}

 
 
  function UploadImage($studentid,$exersiceid,$lessonid){
			$target_dir = "partB/";
		     $target_file = $target_dir  . basename($_FILES["file"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$target_file =$target_dir  . sha1(basename($_FILES["file"]["name"])).$studentid.$exersiceid.$lessonid.".".$imageFileType;
			
			if($imageFileType != "jpg" || $imageFileType != "png" || $imageFileType != "jpeg"
				|| $imageFileType != "gif" || $imageFileType != "docs" || $imageFileType != "mp4") {
				 if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
					return   sha1(basename($_FILES["file"]["name"])).$studentid.$exersiceid.$lessonid.".".$imageFileType;
				}else{
					echo "Error Uploading File";
					exit;
				}
			}else{
					echo "File Not Supported";
					exit;
	 }
} 

 ?>