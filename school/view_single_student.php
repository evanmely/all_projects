<?php
include 'dbconn.php';
$movieArray = array();
$response = array();
//Check for mandatory parameter std_id
if(isset($_GET['id'])){
	$stdid = $_GET['id'];
	//Query to fetch student details
	$query = "SELECT std_id, name, age, class, date_in FROM student_table WHERE std_id=?";
	if($stmt = $con->prepare($query)){
		//Bind std_id parameter to the query
		$stmt->bind_param("i",$stdid);
		$stmt->execute();
		//Bind fetched result to variables 
		$stmt->bind_result($std_id,$name,$age,$class,$date);
		//Check for results		
		if($stmt->fetch()){
			//Populate the student array
			//$studentArray["id"] = $stdid;
			$studentArray["std_id"] = $std_id;
			$studentArray["name"] = $name;
			$studentArray["age"] = $age;
			$studentArray["class"] = $class;
$studentArray["date_in"] = $date;

			$response["success"] = 1;
			$response["data"] = $studentArray;
		
		
		}else{
			//When student is not found
			$response["success"] = 0;
			$response["message"] = "student not found";
		}
		$stmt->close();
 
 
	}else{
		//Whe some error occurs
		$response["success"] = 0;
		$response["message"] = mysqli_error($con);
		
	}
 
}else{
	//When the mandatory parameter movie_id is missing
	$response["success"] = 0;
	$response["message"] = "missing parameter student_id";
}
//Display JSON response
echo json_encode($response);
?>