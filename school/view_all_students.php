<?php
include 'dbconn.php';
//Query to select std id and  name
$query = "SELECT * FROM student_table";
$result = array();
$studentArray = array();
$response = array();
//Prepare the query
if($stmt = $con->prepare($query)){
	$stmt->execute();
	//
	$stmt->bind_result($id,$std_id,$name,$age,$class,$date);
	//Fetch 1 row at a time					
	while($stmt->fetch()){
		//
		$studentArray["id"] = $id;
		$studentArray["std_id"] = $std_id;
		$studentArray["name"] = $name;
		$studentArray["age"] = $age;
		$studentArray["class"] = $class;
		$studentArray["date_in"] = $date;
		$result[]=$studentArray;
		
	}
	$stmt->close();
	$response["success"] = 1;
	$response["data"] = $result;
	
 
}else{
	//Some error while fetching data
	$response["success"] = 0;
	$response["message"] = mysqli_error($con);
		
	
}
//Display JSON response
echo json_encode($response);
 
?>