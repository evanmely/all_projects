<?php
include 'dbconn.php';
$response = array();
 
//Check for mandatory parameters
if(isset($_POST['id'])&&isset($_POST['name'])&&isset($_POST['age'])&&isset($_POST['class'])&&isset($_POST['date'])){
	$student_id = $_POST['id'];
	$student_name = $_POST['name'];
	$student_age = $_POST['age'];
	$student_class = $_POST['class'];
	$adm_date = $_POST['date'];
	
	
	//Query to insert a students
	$query = "INSERT INTO student_table(std_id,name,age,class,date_in) VALUES (?,?,?,?,?)";
	//Prepare the query
	if($stmt = $con->prepare($query)){
		//Bind parameters
		$stmt->bind_param("sssis",$student_id,$student_name,$student_age,$student_class,$adm_date);
		//$stmt->bind_param("ssis",$student_id,$student_name,$student_age,$student_class,$adm_date);
		//Exceting MySQL statement
		$stmt->execute();
		//Check if data got inserted
		if($stmt->affected_rows == 1){
			$response["success"] = 1;			
			$response["message"] = "Student Successfully Added";			
			
		}else{
			//Some error while inserting
			$response["success"] = 0;
			$response["message"] = "Error while adding student";
		}					
	}else{
		//Some error while inserting
		$response["success"] = 0;
		$response["message"] = mysqli_error($con);
	}
 
}else{
	//Mandatory parameters are missing
	$response["success"] = 0;
	$response["message"] = "missing mandatory parameters";
}
//Displaying JSON response
echo json_encode($response);
?>