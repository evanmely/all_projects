let itemImage=document.getElementsByClassName('item-image');
if (itemImage!==null)
{
	for (let i=0;i<itemImage.length;i++)
	{
		itemImage[i].addEventListener('mouseover',event=>{
			let itemDesc=document.getElementsByClassName('item-desc');
			itemDesc[i].children[0].style.display='block';
		});
		itemImage[i].addEventListener('mouseout',event=>{
			let itemDesc=document.getElementsByClassName('item-desc');
			itemDesc[i].children[0].style.display='none';
		});
	}
}
let forms=document.getElementsByClassName('item-desc');
if (forms!==null)
{
	//Prevents form submission
	for (let i=0;i<forms.length;i++)
	{
		forms[i].onsubmit=()=>false;
	}
	let buttons=document.getElementsByClassName('buy');
	for (let i=0;i<buttons.length;i++)
	{
		buttons[i].addEventListener('click',event=>{
			let values=[];
			for (let j=0;j<5;j++)
			{
				values.push(forms[i].elements['item'+j].value);
			}
			let chatbox=document.getElementById('chatbox');
			chatbox.style.display='block';
			document.getElementsByClassName('container')[0].style.opacity="0.2";
			let subject=document.getElementsByClassName('chat-subject')[0];
			document.getElementsByClassName('chat-body')[0].innerHTML="";
			subject.innerHTML=values;
			getChat(localStorage.sender,localStorage.receiver);
		});
	}
}
let chatMinimize=document.getElementById('chat-hide');
chatMinimize.addEventListener('click',event=>{
	let chatbox=document.getElementById('chatbox');
	chatbox.style.display='none';
	document.getElementsByClassName('container')[0].style.opacity="1";
});
let chatForm=document.getElementById('chat-form');
chatForm.onsubmit=()=>false;
let sendChatBtn=document.getElementById('chat-send');
sendChatBtn.addEventListener('click',event=>{
	if (chatForm.checkValidity()===true){
		let textarea=document.getElementById('chat-message')
		let message=textarea.value;
		let view=document.getElementsByClassName('chat-body')[0];
		view.innerHTML+='<div class="sender-message"><p>'+message+'</p></div>';
		chatForm.reset();
		textarea.focus();
		sendChat(localStorage.sender,localStorage.receiver,message);
	}
});
function sendChat(user,receiver,message)
{
	let request=new XMLHttpRequest();
	request.open("POST","chatprocessor.php",true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.onreadystatechange=function(){
		if (this.readyState===4 && this.status===200){
			
		}
	}
	request.send("chatmessage=chat"+"&user="+user+"&receiver="+receiver+"&message="+message);
}
function getChat(sender,receiver){
	let request=new XMLHttpRequest();
	request.open("POST","chatprocessor.php",true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.onreadystatechange=function(){
		if (this.readyState===4 && this.status===200){
			let body=document.getElementsByClassName('chat-body')[0];
			body.innerHTML=this.responseText;
		}
	}
	request.send("getchat=chat"+"&sender="+sender+"&receiver="+receiver);
}
function activeChat(){
	getChat(localStorage.sender,localStorage.receiver);
}
setInterval(activeChat,2000);