<?php
include 'db/db_connect.php';
$response = array();
 
//Check for mandatory parameters
if(isset($_POST['id'])&&isset($_POST['name'])&&isset($_POST['location'])&&isset($_POST['salary'])&&isset($_POST['type'])&&isset($_POST['image'])){
	$HouseHelp_id = $_POST['id'];
	$name = $_POST['name'];
	$location = $_POST['location'];
	$salary = $_POST['salary'];
	$type = $_POST['type'];
	$image = $_POST['image'];
	
	//Query to insert a students
	$query = "INSERT INTO househelps(HouseHelp_id,name,location,expected_salary,type,image) VALUES (?,?,?,?,?,?)";
	//Prepare the query
	if($stmt = $con->prepare($query)){
		//Bind parameters
		$stmt->bind_param("ssssis",$HouseHelp_id,$name,$location,$salary,$type,$image);
		//$stmt->bind_param("ssis",$student_id,$student_name,$student_age,$student_class,$adm_date);
		//Exceting MySQL statement
		$stmt->execute();
		//Check if data got inserted
		if($stmt->affected_rows == 1){
			$response["success"] = 1;			
			$response["message"] = "Student Successfully Added";			
			
		}else{
			//Some error while inserting
			$response["success"] = 0;
			$response["message"] = "Error while adding househelp";
		}					
	}else{
		//Some error while inserting
		$response["success"] = 0;
		$response["message"] = mysqli_error($con);
	}
 
}else{
	//Mandatory parameters are missing
	$response["success"] = 0;
	$response["message"] = "missing mandatory parameters";
}
//Displaying JSON response
echo json_encode($response);
?>